package ncdc.hot.tournamentSystem.dto;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.entities.Tournament;


public class GameDTO {


	private Integer id;
	private Integer gameId;
	private Integer idOfFirstPlayer;
	private Integer idOfSecondPlayer;
	private Integer idWinner;
	private int parentIndex;
	private Integer idStatistics;
	private int whichRound;
	private int gameType;
	private int TournamentId;
	
	public GameDTO(Game game) {
		this.id= game.getId();
		this.gameId=game.getGameId();
		this.idOfFirstPlayer= game.getIdOfFirstPlayer();
		this.idOfSecondPlayer= game.getIdOfSecondPlayer();
		this.idWinner= game.getIdWinner();
		this.whichRound= game.getWhichRound();
		this.gameType=game.getGameType();
		this.idStatistics=game.getIdStatistics();
	}
	
	public Integer getIdStatistics() {
		return idStatistics;
	}

	public void setIdStatistics(Integer idStatistics) {
		this.idStatistics = idStatistics;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	

	public int getWhichRound() {
		return whichRound;
	}

	public void setWhichRound(int whichRound) {
		this.whichRound = whichRound;
	}

	public int getGameType() {
		return gameType;
	}

	public void setGameType(int gameType) {
		this.gameType = gameType;
	}

	public int getTournamentId() {
		return TournamentId;
	}

	public void setTournamentId(int tournamentId) {
		TournamentId = tournamentId;
	}
	
	public Integer getIdOfFirstPlayer() {
		return idOfFirstPlayer;
	}

	public void setIdOfFirstPlayer(Integer idOfFirstPlayer) {
		this.idOfFirstPlayer = idOfFirstPlayer;
	}

	public Integer getIdOfSecondPlayer() {
		return idOfSecondPlayer;
	}

	public void setIdOfSecondPlayer(Integer idOfSecondPlayer) {
		this.idOfSecondPlayer = idOfSecondPlayer;
	}

	public Integer getIdWinner() {
		return idWinner;
	}

	public void setIdWinner(Integer idWinner) {
		this.idWinner = idWinner;
	}

	public int getParentIndex() {
		return parentIndex;
	}

	public void setParentIndex(int parentIndex) {
		this.parentIndex = parentIndex;
	}

	@Override
	public String toString() {
		return "Game [idOfFirstPlayer=" + idOfFirstPlayer
				+ ", idOfSecondPlayer=" + idOfSecondPlayer + ", Winner=" + idWinner + "]";
	}
}
