package ncdc.hot.tournamentSystem.dto;

import ncdc.hot.tournamentSystem.enums.TypeOfGame;

public class BasicGameDTO {

	private Integer idOfFirstPlayer;
	private Integer idOfSecondPlayer;

	private TypeOfGame gameType;

	public Integer getIdOfFirstPlayer() {
		return idOfFirstPlayer;
	}

	public void setIdOfFirstPlayer(Integer idOfFirstPlayer) {
		this.idOfFirstPlayer = idOfFirstPlayer;
	}

	public Integer getIdOfSecondPlayer() {
		return idOfSecondPlayer;
	}

	public void setIdOfSecondPlayer(Integer idOfSecondPlayer) {
		this.idOfSecondPlayer = idOfSecondPlayer;
	}

	public TypeOfGame getGameType() {
		return gameType;
	}

	public void setGameType(TypeOfGame gameType) {
		this.gameType = gameType;
	}
	
	
}
