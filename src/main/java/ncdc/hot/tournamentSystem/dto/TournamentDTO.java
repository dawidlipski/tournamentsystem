package ncdc.hot.tournamentSystem.dto;

import java.util.List;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.enums.TypeOfTournament;
import ncdc.hot.tournamentSystem.models.Player;

public class TournamentDTO {

	private String tournamentName;
	private TypeOfTournament tournamentType;
	private TypeOfGame gameType;
	private boolean ifSolo; // if 1,true = solo, if 0,false = team
	private int numberOfPlayers;
	private int winnerId; // id of player who won final game
	private int height;
	private String date;
	private String description;
	private String rules;
	private List<Game> games;
	private List<Player> players;

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTournamentName() {
		return tournamentName;
	}

	public void setTournamentName(String tournamentName) {
		this.tournamentName = tournamentName;
	}

	public TypeOfTournament getTournamentType() {
		return tournamentType;
	}

	public void setTournamentType(TypeOfTournament tournamentType) {
		this.tournamentType = tournamentType;
	}

	public TypeOfGame getGameType() {
		return gameType;
	}

	public void setGameType(TypeOfGame gameType) {
		this.gameType = gameType;
	}

	public boolean isSolo() {
		return ifSolo;
	}

	public void setIfSolo(boolean ifSolo) {
		this.ifSolo = ifSolo;
	}

	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	public int getWinnerId() {
		return winnerId;
	}

	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	@Override
	public String toString() {
		return "TournamentDTO [tournamentName=" + tournamentName + ", tournamentType=" + tournamentType + ", gameType="
				+ gameType + ", ifSolo=" + ifSolo + ", numberOfPlayers=" + numberOfPlayers + ", winnerId=" + winnerId
				+ ", height=" + height + ", date=" + date + ", description=" + description + ", rules=" + rules
				+ ", games=" + games + ", players=" + players + "]";
	}
	


}