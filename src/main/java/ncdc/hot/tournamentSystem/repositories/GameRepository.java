package ncdc.hot.tournamentSystem.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.entities.Tournament;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;

@Repository
public interface GameRepository extends CrudRepository<Game, Integer> {

	List<Game> findByIdOfFirstPlayer(Integer idOfFirstPlayer);
	List<Game> findByIdOfSecondPlayer(Integer idOfSecondPlayer);
	List<Game> findByTournamentId(Integer idTournament);
	Optional<Game> findByGameId(Integer idGame);
	Optional<Game> findByGameIdAndTournamentId(Integer idGame, Integer idTournament);
}
