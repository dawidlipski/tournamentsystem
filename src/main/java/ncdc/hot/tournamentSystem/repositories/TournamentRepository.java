package ncdc.hot.tournamentSystem.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.entities.Tournament;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;


@Repository
public interface TournamentRepository extends CrudRepository<Tournament, Integer> {
	List<Tournament> findTournamentByGameType(TypeOfGame gameType);
	Optional<Tournament> findTournamentByTournamentName(String tournamentName);
}
