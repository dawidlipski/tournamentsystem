package ncdc.hot.tournamentSystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_ACCEPTABLE, reason="Too many players")
public class PlayerOutOfBoundsException extends Exception {
	private static final long serialVersionUID = 8542571239652947663L;

}
