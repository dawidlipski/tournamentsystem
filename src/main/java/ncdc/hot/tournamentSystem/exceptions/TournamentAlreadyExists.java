package ncdc.hot.tournamentSystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_ACCEPTABLE, reason="Tournament Already Exists")
public class TournamentAlreadyExists extends Exception {
	private static final long serialVersionUID = -1915358376439711344L;

}
