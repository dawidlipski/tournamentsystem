package ncdc.hot.tournamentSystem.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ncdc.hot.tournamentSystem.config.SystemProperties;
import ncdc.hot.tournamentSystem.controller.TournamentController;
import ncdc.hot.tournamentSystem.dto.BasicGameDTO;
import ncdc.hot.tournamentSystem.dto.GameDTO;
import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.entities.Tournament;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.enums.TypeOfTournament;
import ncdc.hot.tournamentSystem.exceptions.GameNotFoundException;
import ncdc.hot.tournamentSystem.logic.GenerateTournamentBracket;
import ncdc.hot.tournamentSystem.models.FinishGame;
import ncdc.hot.tournamentSystem.models.ReturnableGame;
import ncdc.hot.tournamentSystem.repositories.GameRepository;
import ncdc.hot.tournamentSystem.repositories.TournamentRepository;

@Service
public class GameService {

	private static final Logger LOG = Logger.getLogger(TournamentController.class);
	private RestTemplate restTemplate = new RestTemplate();
	private SystemProperties systemProperties;

	@Autowired
	public void setSystemProperties(SystemProperties systemProperties) {
		this.systemProperties = systemProperties;
	}

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private TournamentRepository tournamentRepository;

	public Game generateGameObjectWithIdWinner(FinishGame gameObject, Game copyGameFromDB) {
		copyGameFromDB.setIdStatistics(gameObject.getIdStatistics());
		copyGameFromDB.setIdWinner(gameObject.getIdWinner());

		return copyGameFromDB;
	}

	private void updateUserWonStats(boolean solo, Integer playerId, TypeOfTournament tournamentType,
			TypeOfGame gameType) throws RestClientException {
		String url;
		if (solo) {
			url = systemProperties.getUrlToUserMgMt() + "/user/update/stats/" + playerId + "/"
					+ tournamentType.ordinal() + "/" + gameType.ordinal() + "/won";
		} else {
			url = systemProperties.getUrlToUserMgMt() + "/team/update/stats/" + playerId + "/"
					+ tournamentType.ordinal() + "/" + gameType.ordinal() + "/won";
		}
		String response = restTemplate.postForObject(url, null, String.class);
		LOG.info("Updated user statistics: " + response);
	}

	public static <T> T findMostCommonElement(List<T> list) {
		Map<T, Integer> map = new HashMap<>();

		for (T t : list) {
			Integer val = map.get(t);
			map.put(t, val == null ? 1 : val + 1);
		}

		Entry<T, Integer> max = null;

		for (Entry<T, Integer> e : map.entrySet()) {
			if (max == null || e.getValue() > max.getValue())
				max = e;
		}
		return max.getKey();
	}
	public boolean checkIdWinnerWithIdPlayers(Game gameObject) {
		if(gameObject.getIdWinner().equals(null)) {
			return true;
		}
		if((gameObject.getIdWinner().equals(gameObject.getIdOfFirstPlayer()))
				||(gameObject.getIdWinner().equals(gameObject.getIdOfSecondPlayer()))
				|| (gameObject.getIdWinner().equals(0))
				) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public void checkGameObjectIsZero(Tournament tournamentObject, Game gameObject, Game parent) {
		
	}
	public String finishGame(FinishGame gameInfo) throws GameNotFoundException, RestClientException {

		int idGame = gameInfo.getIdGame();
		Game gameObject = new Game();
		Optional<Game> optionalGame = gameRepository.findById(idGame);
		if (optionalGame.isPresent()) {
			if(optionalGame.get().getIdWinner()==null && optionalGame.get().getIdOfSecondPlayer()!=null) {
				Game gameCopy = optionalGame.get();
				int parentGameId = GenerateTournamentBracket.getParentIndex(gameCopy.getGameId());
				int idTournament = gameCopy.getTournament().getId();

				Optional<Game> parentGame = gameRepository.findByGameIdAndTournamentId(parentGameId, idTournament);
				Optional<Tournament> tournament = tournamentRepository.findById(idTournament);

				Tournament tournamentObject = new Tournament(tournament.get());

				List<Game> gamesWithWinners = gameRepository.findByTournamentId(idTournament);
				List<Integer> listWithWinners = new ArrayList<>();

				if (parentGame.isPresent() && tournament.isPresent()) {
					Game parentObject = new Game(parentGame.get());

					gameObject = generateGameObjectWithIdWinner(gameInfo, gameCopy);
					int winnerId = gameObject.getIdWinner();
					if (tournament.get().getTournamentType().ordinal() == 0) {
						if (gameObject.getGameId().equals(0)) {
							tournamentObject.setWinnerId(winnerId);
							updateUserWonStats(tournamentObject.isIfSolo(), winnerId, tournamentObject.getTournamentType(),
									tournamentObject.getGameType());
							if(checkIdWinnerWithIdPlayers(gameObject)) {
								tournamentRepository.save(tournamentObject);
							}							
						} else {
							parentObject = GenerateTournamentBracket.updateGame(parentObject, gameObject.getIdWinner());
							if(checkIdWinnerWithIdPlayers(gameObject)) {
								gameRepository.save(parentObject);
							}	
						}			
						LOG.info(String.format("Tried update idWinner filed int database ", gameObject.getIdWinner()));
					} else {
						tournamentObject.setWinnerId(generateListWithWinner(listWithWinners, gamesWithWinners, winnerId, tournamentObject));
						if(checkIdWinnerWithIdPlayers(gameObject)) {
							tournamentRepository.save(tournamentObject);
						}
					}
					if(checkIdWinnerWithIdPlayers(gameObject)) {
						gameRepository.save(gameObject);
					}else {
						throw new GameNotFoundException();
					}
					return "Game data updated";
				}
			}else {
				throw new GameNotFoundException();
			}
		}	
		throw new GameNotFoundException();
	}

	public Integer generateListWithWinner(List<Integer> listWithWinners, List<Game> gamesWithWinners, 
			int winnerId, Tournament tournamentObject) {
		
		int playerWin = 0;
		boolean flagForUpdate=true;
		for (Game i : gamesWithWinners) {
			listWithWinners.add(i.getIdWinner());
		}	
		for(Integer i:listWithWinners) {
			if(i == null) {
				flagForUpdate=false;
			}
		}
		listWithWinners.removeAll(Collections.singleton(null));
		if (!listWithWinners.isEmpty()) {
			playerWin = findMostCommonElement(listWithWinners);
		}
		if(flagForUpdate) {
			updateUserWonStats(tournamentObject.isIfSolo(), playerWin, tournamentObject.getTournamentType(),
					tournamentObject.getGameType());
		}
		return playerWin;
	}

	public List<Game> getGamesWithUser(Integer userId) throws GameNotFoundException {
		List<Game> games = gameRepository.findByIdOfFirstPlayer(userId);
		games.addAll(gameRepository.findByIdOfSecondPlayer(userId));

		if (!games.isEmpty()) {
			LOG.info(String.format("Get games by userId: %s", games));
			return games;

		}
		LOG.info(String.format("Tried get games from tournament and NOT FOUND: %s", userId));
		throw new GameNotFoundException();
	}

	public GameDTO findById(Integer id) throws GameNotFoundException {
		Optional<Game> game = gameRepository.findById(id);
		if (game.isPresent()) {
			GameDTO gameDTO= new GameDTO(game.get());
			gameDTO.setTournamentId(game.get().getTournament().getId());
			LOG.info(String.format("Get game by id: %s", gameDTO));
			return gameDTO;
		}
		LOG.info(String.format("Tried get game by id and NOT FOUND: %s", id));
		throw new GameNotFoundException();
	}

	public List<Game> getGamesFromTournament(Integer id) throws GameNotFoundException {

		List<Game> games = tournamentRepository.findById(id).get().getGames();

		if (!games.isEmpty()) {

			LOG.info(String.format("Get games by tournament id: %s", games));
			return games;

		}
		LOG.info(String.format("Tried get games from tournament and NOT FOUND: %s", id));
		throw new GameNotFoundException();
	}

	public List<Game> getGamesByGameType(TypeOfGame gameType) throws GameNotFoundException {

		List<Tournament> tournament = tournamentRepository.findTournamentByGameType(gameType);
		List<Game> aviableGamesWithSelectedType = new ArrayList<>();

		if (!tournament.isEmpty()) {
			LOG.info(String.format("Get games by typeGame: %s", tournament));
			for (Tournament t : tournament) {
				for (Game game : t.getGames()) {
					aviableGamesWithSelectedType.add(game);
				}
			}
			return aviableGamesWithSelectedType;
		}
		throw new GameNotFoundException();

	}

	public List<ReturnableGame> checkIfGamesExist(BasicGameDTO gameInfo) throws GameNotFoundException {
		Integer idOfFirstPlayer = gameInfo.getIdOfFirstPlayer();
		Integer idOfSecondPlayer = gameInfo.getIdOfSecondPlayer();
		TypeOfGame gameType = gameInfo.getGameType();

		List<Game> gamesInGameType = getGamesByGameType(gameType);
		List<ReturnableGame> returnableGames = new ArrayList<>();

		for (Game game : gamesInGameType) {
			if (game == null || game.getIdOfFirstPlayer() == null || game.getIdOfSecondPlayer() == null) {

			} else if ((game.getIdOfFirstPlayer().equals(idOfFirstPlayer)
					&& game.getIdOfSecondPlayer().equals(idOfSecondPlayer))
					|| (game.getIdOfFirstPlayer().equals(idOfSecondPlayer)
							&& game.getIdOfSecondPlayer().equals(idOfFirstPlayer))) {
				if(game.getIdWinner()!=null) {
					
				}else {
					returnableGames.add(mapToReturnableGame(game));
				}
			}
		}
		if (returnableGames.isEmpty())
			throw new GameNotFoundException();
		else
			return returnableGames;
	}

	private ReturnableGame mapToReturnableGame(Game game) {
		ReturnableGame returnableGame = new ReturnableGame();
		returnableGame.setId(game.getId());
		returnableGame.setIdOfFirstPlayer(game.getIdOfFirstPlayer());
		returnableGame.setIdOfSecondPlayer(game.getIdOfSecondPlayer());
		returnableGame.setTournamentId(game.getTournament().getId());
		return returnableGame;
	}
}
