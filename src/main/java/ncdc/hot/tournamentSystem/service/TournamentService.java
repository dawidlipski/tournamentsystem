package ncdc.hot.tournamentSystem.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ncdc.hot.tournamentSystem.config.SystemProperties;
import ncdc.hot.tournamentSystem.controller.TournamentController;
import ncdc.hot.tournamentSystem.dto.TournamentDTO;
import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.entities.Tournament;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.enums.TypeOfTournament;
import ncdc.hot.tournamentSystem.exceptions.GameNotFoundException;
import ncdc.hot.tournamentSystem.exceptions.PlayerOutOfBoundsException;
import ncdc.hot.tournamentSystem.exceptions.TournamentAlreadyExists;
import ncdc.hot.tournamentSystem.exceptions.TournamentNotFound;
import ncdc.hot.tournamentSystem.logic.GenerateTournamentBracket;
import ncdc.hot.tournamentSystem.logic.GenerateTournamentLeague;
import ncdc.hot.tournamentSystem.mappers.TournamentMapper;
import ncdc.hot.tournamentSystem.models.Player;
import ncdc.hot.tournamentSystem.repositories.GameRepository;
import ncdc.hot.tournamentSystem.repositories.TournamentRepository;
import static java.util.Comparator.comparing;
@Service
public class TournamentService {

	private static final Logger LOG = Logger.getLogger(TournamentController.class);
	private RestTemplate restTemplate = new RestTemplate();
	private GenerateTournamentBracket bracketGenerator;
	private SystemProperties systemProperties;
	private GenerateTournamentLeague leagueGenerator;

	@Autowired
	public void setSystemProperties(SystemProperties systemProperties) {
		this.systemProperties = systemProperties;
	}

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private TournamentRepository tournamentRepository;

	private Tournament createTournament(TournamentDTO tournamentDTO) throws PlayerOutOfBoundsException {

		String date = String.valueOf(System.currentTimeMillis());
		Tournament tournament = TournamentMapper.mapToTournament(tournamentDTO);
		List<Player> players = tournamentDTO.getPlayers();
		Collections.shuffle(players);
		if(tournamentDTO.getTournamentType().ordinal()==0) {
			bracketGenerator = new GenerateTournamentBracket(players);
			tournament.setGames(Arrays.asList(bracketGenerator.getGames()));
			tournament.setNumberOfPlayers(players.size());
			tournament.setDate(date);
			int numberOfGames = players.size() - 1;
			tournament.setHeight(GenerateTournamentBracket.getHeight(numberOfGames));
		}else {
			if(players.size()>32) {
				throw new PlayerOutOfBoundsException();
			}
			leagueGenerator= new GenerateTournamentLeague(players);
				
			tournament.setGames(leagueGenerator.getGames());
			tournament.setNumberOfPlayers(players.size());
			tournament.setHeight(leagueGenerator.getRound());
			tournament.setDate(date);
		}
		

		return tournament;
	}

	private void updateUserPlayedStats(Integer userId, TypeOfTournament tournamentType, TypeOfGame gameType)
			throws RestClientException {
		String url = systemProperties.getUrlToUserMgMt() + "/user/update/stats/" + userId + "/"
				+ tournamentType.ordinal() + "/" + gameType.ordinal() + "/played";
		String response = restTemplate.postForObject(url, null, String.class);
		LOG.info("Updated user statistics: " + response);
	}

	public TournamentDTO createGame(TournamentDTO tournamentDTO) throws TournamentAlreadyExists, RestClientException, PlayerOutOfBoundsException {

		LOG.info(String.format("Creating new tournament from data: %s", tournamentDTO));
		Tournament newTournament = createTournament(tournamentDTO);
		newTournament.setDescription(tournamentDTO.getDescription());
		String tournamentName = tournamentDTO.getTournamentName();

		if (tournamentRepository.findTournamentByTournamentName(tournamentName).isPresent()) {
			throw new TournamentAlreadyExists();
		}

		LOG.info(String.format("Saving new tournament to db: %s", tournamentDTO));
		tournamentRepository.save(newTournament);

		LOG.info(String.format("Saving games to db: %s", newTournament.getGames()));
		for (Game game : newTournament.getGames()) {
			game.setTournament(newTournament);
			game.setGameType(newTournament.getGameType().ordinal());
			gameRepository.save(game);
		}

		// send info to user management to increment statistics for each user
		if (tournamentDTO.isSolo()) {
			for (Player player : tournamentDTO.getPlayers()) {
				updateUserPlayedStats(player.getId(), tournamentDTO.getTournamentType(), tournamentDTO.getGameType());
			}
		} else {
			HashMap<String, Player> playerList = new HashMap<>();
			// Update every single user in team
			for (Player player : tournamentDTO.getPlayers()) {
				for (Player user : player.getUsers()) {
					playerList.put(user.getNick(), user);
				}
			}
			List<Player> listWithoutDuplicates = new ArrayList<Player>(playerList.values());
			for (Player player : listWithoutDuplicates) {
				updateUserPlayedStats(player.getId(), tournamentDTO.getTournamentType(), tournamentDTO.getGameType());
			}
		}

		return tournamentDTO;

	}

	public Iterable<Tournament> getAllTournaments() {

		Iterable<Tournament> tournaments = tournamentRepository.findAll();
		LOG.info(String.format("Returning list of all tournaments: %s", tournaments));
		return tournaments;
	}

	public Tournament findById(Integer id) throws TournamentNotFound {
		Optional<Tournament> tournament = tournamentRepository.findById(id);
		if (tournament.isPresent()) {
			List<Game> game= gameRepository.findByTournamentId(id);
			Collections.sort(game, comparing(Game::getId));
			tournament.get().setGames(game);
			LOG.info(String.format("Get tournament by id: %s", tournament));
			return tournament.get();
		}
		LOG.info(String.format("Tried get tournament by id and NOT FOUND: %s", id));
		throw new TournamentNotFound();
	}

	public ResponseEntity<Tournament> updateTournament(Integer id, TournamentDTO tournament) {
		if (this.tournamentRepository.findById(id).isPresent()) {
			Tournament newTournament = TournamentMapper.mapToTournament(tournament);

			tournamentRepository.save(newTournament);

			LOG.info(String.format("Updated tournament: %s", tournament, newTournament));
			return new ResponseEntity<>(newTournament, HttpStatus.OK);

		}
		LOG.info(String.format("Tried to update tournament and NOT FOUND: %s", id));
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	public String deleteTournament(Integer id) throws TournamentNotFound {
		
		if (this.tournamentRepository.findById(id).isPresent()) {
			List<Game> games= gameRepository.findByTournamentId(id);
			for(Game g:games) {
				gameRepository.deleteById(g.getId());
			}
			tournamentRepository.deleteById(id);
			
			LOG.info(String.format("Deleted tournament: %s", id));
			return "Deleted tournament";
		}
		LOG.info(String.format("Tried to delete tournament and NOT FOUND: %s", id));
		throw new TournamentNotFound();

	}

	public List<Tournament> getTournamentsWithUser(Integer userId) throws TournamentNotFound {

		List<Game> games = gameRepository.findByIdOfFirstPlayer(userId);
		games.addAll(gameRepository.findByIdOfSecondPlayer(userId));

		List<Tournament> tournamentsWithUser = new ArrayList<Tournament>();
		int tournamentId = 0;

		for (int i = 0; i < games.size(); i++) {
			tournamentId = games.get(i).getTournament().getId();
			tournamentsWithUser.add(tournamentRepository.findById(tournamentId).get());
		}

		// Filter list
		Set<Tournament> tournamentWithoutDuplicates = new HashSet<>();
		tournamentWithoutDuplicates.addAll(tournamentsWithUser);
		tournamentsWithUser.clear();
		tournamentsWithUser.addAll(tournamentWithoutDuplicates);

		if (!tournamentsWithUser.isEmpty()) {
			LOG.info(String.format("Get tournaments with user: %s", tournamentsWithUser));
			return tournamentsWithUser;
		}
		LOG.info(String.format("Tried get tournaments with user and NOT FOUND: %s", userId));
		throw new TournamentNotFound();
	}

	public List<Tournament> getTournamentByGameType(TypeOfGame gameType) throws GameNotFoundException {

		List<Tournament> tournaments = tournamentRepository.findTournamentByGameType(gameType);

		if (!tournaments.isEmpty()) {
			LOG.info(String.format("Get tournament by typeGame: %s", tournaments));
			return tournaments;
		}
		throw new GameNotFoundException();
	}

}