package ncdc.hot.tournamentSystem.mappers;

import ncdc.hot.tournamentSystem.dto.TournamentDTO;
import ncdc.hot.tournamentSystem.entities.Tournament;

public class TournamentMapper {

	public static Tournament mapToTournament(TournamentDTO tournamentDTO) {
		Tournament tournament= new Tournament();
		
		tournament.setGames(tournamentDTO.getGames());
		tournament.setIfSolo(tournamentDTO.isSolo());
		tournament.setNumberOfPlayers(tournamentDTO.getNumberOfPlayers());
		tournament.setTournamentName(tournamentDTO.getTournamentName());
		tournament.setTournamentType(tournamentDTO.getTournamentType());
		tournament.setGameType(tournamentDTO.getGameType());
		tournament.setWinnerId(tournamentDTO.getWinnerId());
		
		return tournament; 
	}
}
