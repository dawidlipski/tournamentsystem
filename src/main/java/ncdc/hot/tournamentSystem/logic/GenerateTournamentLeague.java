package ncdc.hot.tournamentSystem.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.exceptions.PlayerOutOfBoundsException;
import ncdc.hot.tournamentSystem.models.Player;

public class GenerateTournamentLeague {

	private int numberOfPlayer;
	private int numberOfRounds;
	private int numberOfGamesPerRound;
	private int round=0;
	List<Game> games= new ArrayList<>();
	
	GenerateTournamentLeague(){
		
	}
	
	public GenerateTournamentLeague(List<Player> players) {
		this.numberOfPlayer= players.size();
		this.numberOfRounds= players.size()-1;
		this.numberOfGamesPerRound= players.size()/2;
		GenerateLeague(players);
	}
	
	private void RotateArray(List<Player> listPlayer)
	{
		Player template= listPlayer.get(numberOfPlayer-1);
		listPlayer.remove(numberOfPlayer-1);
	    Collections.rotate(listPlayer, 1);
	    listPlayer.add(template);
	}
	
	private void RotateArrayForOdd(List<Player> listPlayer, Player template)
	{
		Collections.reverse(listPlayer);
		listPlayer.add(template);
		Collections.reverse(listPlayer);
		Collections.rotate(listPlayer, 1);
	}
	
	public List<Game> createSchedule(List<Player> listPlayer){
		Game game= new Game();
		int gameId=0;
		for(round= 0; round< numberOfRounds; round++) {
			for(int i=0;i<numberOfGamesPerRound;i++) {
				int team1= numberOfGamesPerRound-i-1;
				int team2= numberOfGamesPerRound+i;
				game.setIdOfFirstPlayer(listPlayer.get(team1).getId());
				game.setIdOfSecondPlayer(listPlayer.get(team2).getId());
				game.setWhichRound(round);
				game.setGameId(gameId);
				games.add(new Game(game));
				gameId++;
			}
			RotateArray(listPlayer);
		}
		return games;
	}
	
	public List<Game> createScheduleOdd(List<Player> listPlayer){
		Player templateListWithLastPlayer = new Player();
		Game game = new Game();
		int gameId = 0;
		for(round = 0; round < numberOfRounds+1; round++) {
			templateListWithLastPlayer= listPlayer.get(0);
			listPlayer.remove(0);
			for(int i = 0; i < numberOfGamesPerRound; i++) {
				int team1= numberOfGamesPerRound-i-1;
				int team2= numberOfGamesPerRound+i;	
				game.setIdOfFirstPlayer(listPlayer.get(team1).getId());
				game.setIdOfSecondPlayer(listPlayer.get(team2).getId());
				game.setWhichRound(round);
				game.setGameId(gameId);
				games.add(new Game(game));
				gameId++;
			}
			RotateArrayForOdd(listPlayer, templateListWithLastPlayer);
			
		}
		
		return games;
	}
	
	public List<Game> GenerateLeague(List<Player> listPlayer) {
	
		if(listPlayer.size()%2 == 0) {
			return createSchedule(listPlayer);
		}else {
			return createScheduleOdd(listPlayer);
		}
	}
	
	public int getRound() {
		return round;
	}

	public List<Game> getGames(){
		return games;
	}
	
	
//	public static void main(String[] args) {
//		List<Player> players= new ArrayList<>();
//		for(int i = 1; i <= 8;i++) {
//			players.add(new Player(i));
//		}
//		GenerateTournamentLeague generateTournamentLeague= new GenerateTournamentLeague(players);
//		for(Game d:generateTournamentLeague.getGames()) {
//			System.out.println("Runda: "+d.getWhichRound()+" Gra: "+d.getGameId()+" Player1: "+d.getIdOfFirstPlayer()+" vs "+d.getIdOfSecondPlayer());
//		}
//	}
}
