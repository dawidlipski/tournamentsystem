package ncdc.hot.tournamentSystem.logic;

import java.util.ArrayList;
import java.util.List;

import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.models.Player;

public class GenerateTournamentBracket {

	private int numberOfPlayers;
	private int numberOfGames;
	Game[] games;

	GenerateTournamentBracket() {
	}

	public GenerateTournamentBracket(List<Player> players) {
		this.numberOfPlayers = players.size();
		this.numberOfGames = numberOfPlayers - 1;
		int size = getRequiredArraySize(numberOfGames);
		games = new Game[size];
		setPlayers(players, 0); // start
		replaceNullWithGames(games);
	}

	public void setPlayers(List<Player> players, int gameIndex) { // set array with players

		games[gameIndex] = new Game();
		games[gameIndex].setGameId(gameIndex);
		games[gameIndex].setWhichRound(getRound(gameIndex));

		if (players == null || players.size() == 0) {
			// do nothing, dont set players
			// because there arent any players

		} else if (players.size() == 2) {

			// Save players to leaf
			games[gameIndex].setIdOfFirstPlayer(players.get(0).getId());
			games[gameIndex].setIdOfSecondPlayer(players.get(1).getId());

		} else if (players.size() == 3) {

			// When passing 3 players, keep one in current game and save two others to the
			// next game
			games[gameIndex].setIdOfFirstPlayer(players.get(0).getId());
			setPlayers(players.subList(1, 3), getLeftChildIndex(gameIndex));

		} else {

			setPlayers(players.subList(0, (players.size() / 2)), getLeftChildIndex(gameIndex));
			setPlayers(players.subList((players.size() / 2), players.size()), getRightChildIndex(gameIndex));
		}

	}

	public int getRound(int gameIndex) {

		Game currentGame = games[gameIndex];
		int counter = 0;

		while (gameIndex != 0) {
			counter++;
			gameIndex = getParentIndex(gameIndex);
		}
		return counter;
	}

	private Game[] replaceNullWithGames(Game[] games) {

		for (int i = 0; i < games.length; i++) {
			if (games[i] == null) {
				games[i] = new Game();
				games[i].setWhichRound(getRound(i));
			}
		}
		return games;
	}

	public Game[] getGames() {
		return games;
	}

	public static Game updateGame(Game parent, int winnerId) {

		if (parent.getIdOfFirstPlayer() == null) {
			parent.setIdOfFirstPlayer(winnerId);
		} else {
			parent.setIdOfSecondPlayer(winnerId);
		}

		return parent;
	}

	// Helper methods

	public static int getRequiredArraySize(int numberOfGames) {
		int requiredHeight = getHeight(numberOfGames);
		return (int) Math.pow(2, requiredHeight) - 1; // subtracts one because array starts at 0
	}

	public static int getHeight(int numberOfGames) // taken from geeksforgeeks.org
	{
		return (int) Math.ceil(Math.log(numberOfGames + 1) / Math.log(2));
	}

	public static int getHeightOfGame(int gameIndex) // taken from geeksforgeeks.org
	{
		if (gameIndex == 0 || gameIndex == 1) {
			return (int) Math.ceil(Math.log(gameIndex + 1) / Math.log(2));
		} else {
			return (int) Math.ceil(Math.log(gameIndex + 1) / Math.log(2)) - 1;
		}
	}

	private boolean hasLeftChild(int index) {
		return getLeftChildIndex(index) < numberOfGames;
	}

	private boolean hasRightChild(int index) {
		return getRightChildIndex(index) < numberOfGames;
	}

	private boolean hasParent(int index) {
		return getParentIndex(index) > 0;
	}

	public static int getLeftChildIndex(int parentIndex) {
		return 2 * parentIndex + 1;
	}

	public static int getRightChildIndex(int parentIndex) {
		return 2 * parentIndex + 2;
	}

	public static int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}

	private Game getLeftChild(int index) {
		return games[getLeftChildIndex(index)];
	}

	private Game getRightChild(int index) {
		return games[getRightChildIndex(index)];
	}

	private Game getParent(int index) {
		return games[getParentIndex(index)];
	}

	public Game getRoot() {
		if (numberOfPlayers == 0)
			throw new IllegalStateException();
		return games[0];
	}
}
