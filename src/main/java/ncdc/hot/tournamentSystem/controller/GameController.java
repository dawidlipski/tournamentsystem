package ncdc.hot.tournamentSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import ncdc.hot.tournamentSystem.dto.BasicGameDTO;
import ncdc.hot.tournamentSystem.dto.GameDTO;
import ncdc.hot.tournamentSystem.entities.Game;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.exceptions.GameNotFoundException;
import ncdc.hot.tournamentSystem.models.FinishGame;
import ncdc.hot.tournamentSystem.models.ReturnableGame;
import ncdc.hot.tournamentSystem.service.GameService;

@RestController
@RequestMapping("/game")
public class GameController {

	@Autowired
	private GameService gameService;

	@GetMapping("/get-from/{id}")
	public ResponseEntity<List<Game>> getGamesFromTournament(@PathVariable Integer id) {
		try {
			List<Game> game = gameService.getGamesFromTournament(id);
			return new ResponseEntity<>(game, HttpStatus.OK);
		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/get/{id}")
	public ResponseEntity<GameDTO> findById(@PathVariable Integer id) {
		try {
			GameDTO game = gameService.findById(id);
			return new ResponseEntity<>(game, HttpStatus.OK);
		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/get/with-user/{id}")
	public ResponseEntity<List<Game>> getGamesWithUser(@PathVariable Integer userId) {
		try {
			List<Game> game = gameService.getGamesWithUser(userId);
			return new ResponseEntity<>(game, HttpStatus.OK);
		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/finish")
	public ResponseEntity<String> finishGame(@RequestBody FinishGame game) {

		if (game.getIdGame() != null && game.getIdStatistics() != null && game.getIdWinner() != null) {

			try {
				String result = gameService.finishGame(game);
				return new ResponseEntity<>(result, HttpStatus.OK);
			} catch (GameNotFoundException e) {
				return new ResponseEntity<>("Not found game", HttpStatus.NOT_FOUND);
			} catch (RestClientException e) {
				return new ResponseEntity<>("Error during communication", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("Not enough data. Send: idGame, idStatistics and idWinner",
					HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/get/with-gametype/{gameType}")
	public ResponseEntity<List<Game>> getGamesByGameType(@PathVariable TypeOfGame gameType) {
		try {
			List<Game> game = gameService.getGamesByGameType(gameType);
			return new ResponseEntity<>(game, HttpStatus.OK);
		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/*
	 * Method for foosball and others to check if game exists
	 */
	@PostMapping("/check")
	public ResponseEntity<List<ReturnableGame>> checkIfGameExists(@RequestBody BasicGameDTO gameInfo) {
		try {
			List<ReturnableGame> games = gameService.checkIfGamesExist(gameInfo);
			return new ResponseEntity<>(games, HttpStatus.OK);

		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
