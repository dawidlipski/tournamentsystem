package ncdc.hot.tournamentSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import ncdc.hot.tournamentSystem.dto.TournamentDTO;
import ncdc.hot.tournamentSystem.entities.Tournament;
import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.exceptions.GameNotFoundException;
import ncdc.hot.tournamentSystem.exceptions.PlayerOutOfBoundsException;
import ncdc.hot.tournamentSystem.exceptions.TournamentAlreadyExists;
import ncdc.hot.tournamentSystem.exceptions.TournamentNotFound;
import ncdc.hot.tournamentSystem.service.TournamentService;

@RestController
@RequestMapping("/tournament")
public class TournamentController {

	@Autowired
	private TournamentService tournamentService;

	@PostMapping("/create")
	public ResponseEntity<TournamentDTO> createGame(@RequestBody TournamentDTO tournamentDTO) throws PlayerOutOfBoundsException {
		TournamentDTO tournament;
		try {
			tournament = tournamentService.createGame(tournamentDTO);
			return new ResponseEntity<>(tournament, HttpStatus.CREATED);
		} catch (TournamentAlreadyExists e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (RestClientException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (PlayerOutOfBoundsException e){
			throw e;
		}
	}

	@GetMapping(path = "/all")
	public ResponseEntity<Iterable<Tournament>> getAllTournaments() {
		Iterable<Tournament> tournament = tournamentService.getAllTournaments();
		return new ResponseEntity<>(tournament, HttpStatus.OK);
	}

	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Tournament> findById(@PathVariable Integer id) {
		try {
			Tournament tournament = tournamentService.findById(id);
			return new ResponseEntity<>(tournament, HttpStatus.OK);
		} catch (TournamentNotFound d) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Tournament> updateTournament(@PathVariable Integer id,
			@RequestBody TournamentDTO tournament) {
		return tournamentService.updateTournament(id, tournament);
	}

	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<String> deleteTournament(@PathVariable Integer id) {
		try {
			String result = tournamentService.deleteTournament(id);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (TournamentNotFound e) {
			return new ResponseEntity<>("Tried to delete tournament and NOT FOUND", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/get/with-user/{userId}")
	public ResponseEntity<List<Tournament>> getTournamentsWithUser(@PathVariable Integer userId) {
		try {
			List<Tournament> tournament = tournamentService.getTournamentsWithUser(userId);
			return new ResponseEntity<>(tournament, HttpStatus.OK);
		} catch (TournamentNotFound e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/get/with-gametype/{gameType}")
	public ResponseEntity<List<Tournament>> getGamesByGameType(@PathVariable TypeOfGame gameType) {
		try {
			List<Tournament> tournaments = tournamentService.getTournamentByGameType(gameType);
			return new ResponseEntity<>(tournaments, HttpStatus.OK);
		} catch (GameNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
