package ncdc.hot.tournamentSystem.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import ncdc.hot.tournamentSystem.enums.TypeOfGame;
import ncdc.hot.tournamentSystem.enums.TypeOfTournament;

@Entity
public class Tournament {

	@Id
	@GeneratedValue
	private int id;
	private String tournamentName;
	private TypeOfTournament tournamentType;
	private TypeOfGame gameType;
	private boolean ifSolo; // if 1 = solo, if 0 = team
	private int numberOfPlayers;
	private int winnerId; // id of player who won final game
	private int height;
	private String date;
	@Size(min=0, max=250)
	private String description;
	@Size(min=0, max=250)
	private String rules;
	@OneToMany(mappedBy = "tournament")
	private List<Game> games;

	public Tournament() {

	}

	public Tournament(int id, String tournamentName, TypeOfTournament tournamentType, TypeOfGame gameType,
			boolean ifSolo, int numberOfPlayers, int winnerId, List<Game> games, String date, String rules, String description) {

		this.id = id;
		this.tournamentName = tournamentName;
		this.tournamentType = tournamentType;
		this.gameType = gameType;
		this.ifSolo = ifSolo;
		this.numberOfPlayers = numberOfPlayers;
		this.winnerId = winnerId;
		this.games = games;
		this.date= date;
		this.rules= rules;
		this.description= description;
	}
	
	

	public Tournament(Tournament tournament) {

		this.id = tournament.getId();
		this.tournamentName = tournament.getTournamentName();
		this.tournamentType = tournament.getTournamentType();
		this.gameType = tournament.getGameType();
		this.ifSolo = tournament.isIfSolo();
		this.numberOfPlayers = tournament.getNumberOfPlayers();
		this.winnerId = tournament.getWinnerId();
		this.games = tournament.getGames();
		this.height= tournament .getHeight();
		this.date= tournament.getDate();
		this.description= tournament.getDescription();
		this.rules= tournament.getRules();
	}
	
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTournamentName() {
		return tournamentName;
	}

	public void setTournamentName(String tournamentName) {
		this.tournamentName = tournamentName;
	}

	public TypeOfTournament getTournamentType() {
		return tournamentType;
	}

	public void setTournamentType(TypeOfTournament tournamentType) {
		this.tournamentType = tournamentType;
	}

	public TypeOfGame getGameType() {
		return gameType;
	}

	public void setGameType(TypeOfGame gameType) {
		this.gameType = gameType;
	}

	public boolean isIfSolo() {
		return ifSolo;
	}

	public void setIfSolo(boolean ifSolo) {
		this.ifSolo = ifSolo;
	}

	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	public int getWinnerId() {
		return winnerId;
	}

	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}
}
