package ncdc.hot.tournamentSystem.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ncdc.hot.tournamentSystem.enums.TypeOfGame;

@Entity
public class Game {

	@Id
	@GeneratedValue
	private Integer id;

	private Integer gameId;
	
	@ManyToOne
	@JsonIgnore
	private Tournament tournament;
	private Integer idOfFirstPlayer;
	private Integer idOfSecondPlayer;
	private Integer idWinner;
	private Integer idStatistics;
	private int whichRound;
	private int gameType; 
	
	public Game() {
		
	}
	public Game(Game game) {
		this.id= game.getId();
		this.gameId=game.getGameId();
		this.idOfFirstPlayer= game.getIdOfFirstPlayer();
		this.idOfSecondPlayer= game.getIdOfSecondPlayer();
		this.idWinner= game.getIdWinner();
		this.tournament= game.getTournament();
		this.whichRound= game.getWhichRound();
		this.gameType=game.getGameType();
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public int getWhichRound() {
		return whichRound;
	}

	public void setWhichRound(int whichRound) {
		this.whichRound = whichRound;
	}

	public Integer getIdOfFirstPlayer() {
		return idOfFirstPlayer;
	}

	public void setIdOfFirstPlayer(Integer idOfFirstPlayer) {
		this.idOfFirstPlayer = idOfFirstPlayer;
	}

	public Integer getIdOfSecondPlayer() {
		return idOfSecondPlayer;
	}

	public void setIdOfSecondPlayer(Integer idOfSecondPlayer) {
		this.idOfSecondPlayer = idOfSecondPlayer;
	}

	public Integer getIdWinner() {
		return idWinner;
	}

	public void setIdWinner(Integer idWinner) {
		this.idWinner = idWinner;
	}

	public Integer getIdStatistics() {
		return idStatistics;
	}
	public void setIdStatistics(Integer idStatistics) {
		this.idStatistics = idStatistics;
	}
	public int getGameType() {
		return gameType;
	}
	public void setGameType(int gameType) {
		this.gameType = gameType;
	}
}
