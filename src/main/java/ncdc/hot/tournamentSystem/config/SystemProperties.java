package ncdc.hot.tournamentSystem.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:connecting.properties")
public class SystemProperties {

	@Value("${url-usrmgmt}")
	private String urlToUserMgMt;
	@Value("${url-foosball}")
	private String urlToFoosball;
	@Value("${url-turn-based}")
	private String urlToTurnBased;

	public String getUrlToUserMgMt() {
		return urlToUserMgMt;
	}

	public String getUrlToFoosball() {
		return urlToFoosball;
	}

	public String getUrlToTurnBased() {
		return urlToTurnBased;
	}
}
