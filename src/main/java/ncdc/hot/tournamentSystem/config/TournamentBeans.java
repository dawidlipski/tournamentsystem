package ncdc.hot.tournamentSystem.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TournamentBeans {
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder rtb) {
		return rtb.build();
	}

}
