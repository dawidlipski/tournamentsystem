package ncdc.hot.tournamentSystem.models;

public class ReturnableGame {

	private Integer id;
	private Integer tournamentId;

	private Integer idOfFirstPlayer;
	private Integer idOfSecondPlayer;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(Integer tournamentId) {
		this.tournamentId = tournamentId;
	}

	public Integer getIdOfFirstPlayer() {
		return idOfFirstPlayer;
	}

	public void setIdOfFirstPlayer(Integer idOfFirstPlayer) {
		this.idOfFirstPlayer = idOfFirstPlayer;
	}

	public Integer getIdOfSecondPlayer() {
		return idOfSecondPlayer;
	}

	public void setIdOfSecondPlayer(Integer idOfSecondPlayer) {
		this.idOfSecondPlayer = idOfSecondPlayer;
	}

}
