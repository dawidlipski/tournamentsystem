package ncdc.hot.tournamentSystem.models;

import java.util.List;

public class Player {

	private Integer id;
	private String name;
	private String surname;
	private String email;
	private String nick;
	private Integer idGroup;
	private String bot;
	private String cardId;
	private List<Player> users;

	public Player() {
		
	}

	public Player(int id, String nick) {
		this.id = id;
		this.nick = nick;
	}

	public Player(int id) {
		this.id = id;
	}

	public Player(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public Integer getId() {
		return id;
	}

	public Integer getIdGroup() {
		return idGroup;
	}

	public String getSurname() {
		return surname;
	}

	public String getNick() {
		return nick;
	}

	public void setIdGroup(Integer idGroup) {
		this.idGroup = idGroup;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getBot() {
		return bot;
	}

	public void setBot(String bot) {
		this.bot = bot;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public List<Player> getUsers() {
		return users;
	}

	public void setUsers(List<Player> users) {
		this.users = users;
	}

}