package ncdc.hot.tournamentSystem.models;

public class FinishGame {

	private Integer idGame;
	private Integer idStatistics;
	private Integer idWinner;
	
	public Integer getIdGame() {
		return idGame;
	}
	public void setIdGame(Integer idGame) {
		this.idGame = idGame;
	}
	public Integer getIdStatistics() {
		return idStatistics;
	}
	public void setIdStatistics(Integer idStatistics) {
		this.idStatistics = idStatistics;
	}
	public Integer getIdWinner() {
		return idWinner;
	}
	public void setIdWinner(Integer idWinner) {
		this.idWinner = idWinner;
	}
	
	
}
